<?php
declare(strict_types = 1);

require_once __DIR__ . '/clases/Juego.php';
require_once __DIR__ . '/clases/Personaje.php';
require_once __DIR__ . '/clases/Lenador.php';
require_once __DIR__ . '/clases/Arquero.php';

$arquero = new Arquero;
$lenador = new Lenador;
/*
$juego = new Juego;
$juego->ubicar($arquero);
*/

$juego = new Juego;
$juego->combatir($arquero, $lenador);


/*
$arquero = new Arquero;
var_dump($arquero);
*/

/*
$lenador = new Lenador;
var_dump($lenador);

function  mover (Lenador $personaje) {
  $personaje->mover(20, 30);
}

mover($lenador);
*/
