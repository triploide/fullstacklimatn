<?php
require_once __DIR__ . '/Personaje.php';

class Lenador extends Personaje
{
  private $hacha;

  public function hacerDanio(Personaje $personaje)
  {
    $danio = 1;
    $atinarHacha = rand(1,3);
    if ($atinarHacha == 1) {
      $danio += 5;
      echo 'Hachazo!<br>';
    }
    echo 'Leñador hace daño y tiene '. $this->getVida().' de vida<br>';
    $personaje->recibirDanio($danio);
  }

  public function recibirDanio($danio)
  {
    $this->vida -= $danio;
  }
}
