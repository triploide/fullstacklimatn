<?php
require_once __DIR__ . '/Personaje.php';
require_once __DIR__ . '/Movible.php';

class Juego
{
  public function combatir(Personaje $personaje1, Personaje $personaje2)
  {
    do {
      $personaje1->hacerDanio($personaje2);
      if ($personaje2->getVida() > 0) {
        $personaje2->hacerDanio($personaje1);
      }
    } while ($personaje1->getVida() > 0  && $personaje2->getVida() > 0);
  }

  function  ubicar(Movible $movible) {
    $movible->mover(20, 30);
  }
}
