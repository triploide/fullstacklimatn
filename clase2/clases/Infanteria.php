<?php
require_once __DIR__ . '/Personaje.php';

class Infanteria extends Personaje
{
  private $espada;

  public function hacerDanio(Personaje $personaje)
  {
    $personaje->recibirDanio(1);
  }

  public function recibirDanio($danio)
  {
    $this->vida -= $danio;
  }
}
