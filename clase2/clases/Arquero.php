<?php
require_once __DIR__ . '/Movible.php';
require_once __DIR__ . '/Personaje.php';

class Arquero extends Personaje implements Movible
{
  private $arco;

  public function __construct()
  {
    parent::__construct();
    $this->vida = 200;
  }

  public function hacerDanio(Personaje $personaje)
  {
    $atinar = rand(1, 5);
    if ($atinar == 1) {
      $personaje->recibirDanio(20);
      echo 'Arquero - Flechazo! y tiene'. $this->getVida().' de vida<br>';
    }
  }

  public function recibirDanio($danio)
  {
    $this->vida -= $danio;
  }
}
