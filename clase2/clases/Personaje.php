<?php

abstract class Personaje
{
  protected $vida;
  protected $x;
  protected $y;

  public function __construct()
  {
    $this->vida = 100;
    $this->x = rand(1, 10);
    $this->y = rand(1, 10);
  }

  public function getVida()
  {
    return $this->vida;
  }

  public function mover($x, $y)
  {
    $this->x = $x;
    $this->y = $y;
    echo 'moviendo...<br>';
  }

  public abstract function hacerDanio(Personaje $personaje);

  public abstract function recibirDanio($danio);
}
