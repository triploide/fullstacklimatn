<?php
declare(strict_types = 1);
require_once __DIR__ . '/Arbol.php';

class Usuario
{
  private $nombre;
  private $email;
  private $pass;
  private $arbol;

  public function __construct(string $nombre, string $email, int $pass)
  {
    $this->setNombre($nombre);
    $this->setEmail($email);
    $this->setPass($pass);
  }

  //array, bool, string, int, float, callable

  public function setArbol(Arbol $arbol)
  {
    $this->arbol = $arbol;
  }

  public function getArbol(): Arbol
  {
    return $this->arbol;
  }

  public function setNombre($nombre)
  {
    $this->nombre = $nombre;
  }

  public function setEmail($email)
  {
    $this->email = $email;
  }

  public function setPass($pass)
  {
    $this->pass = $pass;
  }

}
