<?php
declare(strict_types = 1);

class Arbol
{
  private $x;
  private $y;
  private $madera;

  public function __construct($madera, $x, $y)
  {
    $this->madera = $madera;
    $this->ubicar($x, $y);
  }

  public function getMadera()
  {
    return $this->madera;
  }

  public function ubicar($x, $y)
  {
    $this->x = $x;
    $this->y = $y;
  }

  public function obtenerMadera($cantidad)
  {
    $this->madera = $this->madera - $cantidad;
    return $cantidad;
    //$this->madera -= $cantidad;
  }









}
