<?php
declare(strict_types = 1);

require "clases/Usuario.php";
require_once "clases/Arbol.php";
$lisa = new Usuario('Lisa', 'lisa@mail.com', 123);
var_dump($lisa);

$peral = new Arbol(100, 10, 30);
$lisa->setArbol($peral);

var_dump($lisa->getArbol()->getMadera());

/*
require "clases/Arbol.php";

$arbol = new Arbol(100, 30, 30);
//$arbol->ubicar(30, 45);

var_dump($arbol->obtenerMadera(10));
var_dump($arbol);
*/
